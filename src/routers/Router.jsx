import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { Contact, Home } from '../pages';
import { PublicRoute } from './PublicRoute';
import { Header, Footer } from '../components/common';
import { LayoutFull } from '../layout/LayoutFull';

export const Routers = () => {
  return (
    <Router>
      <Switch>
        <PublicRoute
          exact
          path="/"
          component={Home}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={Header}
          footer={Footer}
        />
        <PublicRoute
          exact
          path="/contact"
          component={Contact}
          layout={LayoutFull}
          isHasFooter={true}
          isHasHeader={true}
          header={Header}
          footer={Footer}
        />
      </Switch>
    </Router>
  );
};
