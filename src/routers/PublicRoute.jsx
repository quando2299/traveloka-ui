import React from 'react';
import { Route } from 'react-router-dom';

export const PublicRoute = ({
  component: Component,
  layout: Layout,
  header: Header,
  footer: Footer,
  isHasHeader,
  isHasFooter,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout Header={isHasHeader ? Header : ''} Footer={isHasFooter ? Footer : ''}>
          <Component {...props} />
        </Layout>
      )}
    />
  );
};
