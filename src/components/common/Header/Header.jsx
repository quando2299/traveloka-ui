import React from 'react';

export const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <div className="row">
          <div className="header__left">Header Left</div>
          <div className="header__right">Header Right</div>
        </div>
      </div>
    </header>
  );
};
