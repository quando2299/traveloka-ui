import {
  GET_ALL_HOTELS_SUCCESS,
  GET_ALL_HOTELS_FAILURE,
  GET_ALL_HOTELS_REQUEST,
} from '../constants/hotel';

const initialState = {
  isLoading: false,
  data: [],
  err: {},
};

const hotelReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_HOTELS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case GET_ALL_HOTELS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
      };
    case GET_ALL_HOTELS_FAILURE:
      return {
        ...state,
        err: action.payload,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default hotelReducers;
