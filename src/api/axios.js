import axios from 'axios';

const baseURL = process.env.BASE_URL;
// const token = ??? Chỗ này em lấy token đã lưu ra

const myAxios = axios.create({
  baseURL: baseURL + 'api/',
  headers: {
    'content-type': 'application/json',
    Authorization: token,
  },
});
axiosMy.interceptors.response.use(
  (res) => {
    if (res.data.message === 'Không có người dùng tương ứng. Vui lòng thử lại.') {
      //   logout user ra hoặc hiện thông báo
    }
    return res;
  },
  (err) => {
    if (err.response.status === 401) {
      //   logout user ra hoặc hiện thông báo
    }

    throw err;
  },
);

export default myAxios;
